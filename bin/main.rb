require_relative '../lib/bill.rb'
require_relative '../lib/product.rb'
Y_REGEX = /^y$/i
N_REGEX = /^n$/i
bill = Bill.new
continue = ""
while not continue =~ N_REGEX
  print "Name of the product: "
  name = gets.chomp
  print "Imported? "
  imported = gets.chomp
  print "Exempted from sales tax? "
  exempted_from_sale_tax = gets.chomp
  print "Price: "
  price = gets.chomp
  print "Do you want to add more items to your list(y/n): "
  continue = gets.chomp
  p exempted_from_sale_tax
  bill.add_item(Product.new(name, imported, exempted_from_sale_tax, price))
  continue =~ Y_REGEX ? next : print(bill.generate)
end

