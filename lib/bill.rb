class Bill
  @@item_list = []

  def add_item(item)
    @@item_list.push(item)
  end

  def generate
    bill = "S.No. |Item Name           |Imported Tax|Sale Tax    |Price       |Total Price \n"
    bill_width = bill.length
    bill += "-" * bill_width + "\n"
    @@item_list.each_with_index do |item, index|
      bill += adjust_amount((index+1).to_s, 6) + "|"
      bill += adjust_string(item.name, 20) + "|"
      bill += adjust_amount(item.import_tax, 12) + "|"
      bill += adjust_amount(item.sale_tax, 12) + "|"
      bill += adjust_amount(item.price, 12) + "|"
      bill += adjust_amount(item.total_price, 12) + "\n"
    end
    bill += "-" * bill_width + "\n"
    bill += "Grand Total " + " " * 54 + "|" + adjust_amount(grand_total, 12)  + "\n"
  end

  def grand_total
    @@item_list.inject(0) { |total, item| total += item.total_price }.round
  end

  def adjust_string(string, length)
    string.length > length ? (string = string[0..length-4] + "...") : string.ljust(length)
  end

  def adjust_amount(amount, length)
    amount.to_s.rjust(length)
  end
end

