class Product

  YES_REGEX = /^yes$/i
  attr_reader :name, :imported, :extempted_from_sale_tax, :price

  def initialize(name, imported, extempted_for_sale_tax, price)
    @name = name
    @imported = imported
    @extempted_from_sale_tax = extempted_for_sale_tax
    @price = price.to_f.round(2)
  end

  def import_tax
    imported =~ YES_REGEX ? (price * 0.05).round(2) : 0.00
  end

  def sale_tax
    extempted_from_sale_tax =~ YES_REGEX ? 0.00 : (price * 0.10).round(2)
  end

  def total_price
    (import_tax + sale_tax + price).round(2)
  end
end

